package SwingThread1;

import javax.swing.JTextArea;

/**
 * @author pacoaldarias <paco.aldarias@ceedcv.es>
 */
class Test extends Thread {

  private String nombre;
  private int retardo;
  private JTextArea jTextArea;

  // Constructor para almacenar nuestro nombre
  // y el retardo
  public Test(String s, int d, JTextArea t) {
    nombre = s;
    retardo = d;
    jTextArea = t;
  }

  // El metodo run() es similar al main(), pero para
  // threads. Cuando run() termina el thread muere
  @Override
  public void run() {
    // Retasamos la ejecución el tiempo especificado
    try {
      mostrarStatus("Iniciando " + nombre);

      Parar / Sleep el tiempo que pone reatardo


    } catch (InterruptedException e) {
    }

    // Ahora imprimimos el nombre
    mostrarStatus("Fin " + nombre + " " + retardo);
  }

  public void mostrarStatus(String string) {

    Mostrar string en jTextArea

  }
}
