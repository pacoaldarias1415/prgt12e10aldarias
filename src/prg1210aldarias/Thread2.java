package prg1210aldarias;

class Thread2 implements Runnable {

  Thread thread;

  Thread2() {
    thread = new Thread(this);
    thread.start();
  }

  public void run() {
    // Instrucciones para el que se creo
    System.out.println("Hilo " + thread.getName());
  }

  public static void main(String args[]) {
    // Creamos los hilos
    Thread2 s1 = new Thread2();
    Thread2 s2 = new Thread2();
  }
}
/* EJECUCION
 Hilo Thread-0
 Hilo Thread-1
 */
