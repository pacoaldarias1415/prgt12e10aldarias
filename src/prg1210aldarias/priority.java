package prg1210aldarias;

class Counter implements Runnable {

  Thread thread;
  int counter = 0;
  volatile boolean goflag;

  public Counter(int p) {
    thread = new Thread(this);
    thread.setPriority(p);
  }

  public void start() {
    goflag = true;
    thread.start();
  }

  public void run() {
    while (goflag) {
      counter++;
    }
  }

  public void end() {
    goflag = false;
  }
}

class priority {

  public static void main(String args[]) {
    System.out.println("Thread.MAX_PRIORITY : "
            + Thread.MAX_PRIORITY);
    System.out.println("Thread.NORM_PRIORITY:  "
            + Thread.NORM_PRIORITY);
    System.out.println("Thread.MIN_PRIORITY :  "
            + Thread.MIN_PRIORITY);
    Counter thread1 = new Counter(Thread.MAX_PRIORITY);
    Counter thread2 = new Counter(Thread.MIN_PRIORITY);
    thread1.start();
    thread2.start();
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
    }
    thread1.end();
    thread2.end();
    System.out.println("Hilo 1 contado: " + thread1.counter);
    System.out.println("Hilo 2 contado: " + thread2.counter);
  }
}
/* EJEMPLO:
 Thread.MAX_PRIORITY : 10
 Thread.NORM_PRIORITY:  5
 Thread.MIN_PRIORITY :  1
 Hilo 1 contado: 1753767508
 Hilo 2 contado: 1720629435
 * */
