package prg1210aldarias;

class SecondThread implements Runnable {

  Thread thread;

  SecondThread() {
    thread = new Thread(this, "segundo");
    System.out.println("Iniciando segundo hilo");
    thread.start();
  }

  public void run() {
    try {
      for (int loopindex = 0; loopindex < 4; loopindex++) {
        System.out.println((Thread.currentThread()).getName()
                + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (Exception e) {
    }
    System.out.println("Final del segundo hilo.");
  }

  public static void main(String args[]) {
    SecondThread secondthread = new SecondThread();
    try {
      for (int loopindex = 0; loopindex < 2; loopindex++) {
        System.out.println((Thread.currentThread()).getName()
                + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
    }
  }
}

/* EJEMPLO:
 Iniciando segundo hilo
 main hilo aqui...
 segundo hilo aqui...
 main hilo aqui...
 segundo hilo aqui...
 Final del segundo hilo.
 */
