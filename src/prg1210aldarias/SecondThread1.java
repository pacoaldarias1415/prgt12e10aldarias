class SecondThread1 extends Thread {
  SecondThread1() {
    super("segundo");
    start();
  }

  public void run() {
    try {
      for(int loop_index = 0; loop_index < 2; loop_index++) {
        System.out.println((Thread.currentThread()).getName()
                           + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {}
    System.out.println("Segundo hilo finalizando.");
  }

  public static void main(String args[]) {
    SecondThread1 secondthread = new SecondThread1();
    try {
      for(int loop_index = 0; loop_index < 2; loop_index++) {
        System.out.println((Thread.currentThread()).getName()
                           + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {}
  }
}
/* EJECUCION:
main hilo aqui...
segundo hilo aqui...
main hilo aqui...
segundo hilo aqui...
Segundo hilo finalizando.
*/
