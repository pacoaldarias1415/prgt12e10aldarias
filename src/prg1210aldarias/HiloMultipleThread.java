class HiloMultipleThread extends Thread {
	
   HiloMultipleThread(String name) {
      super(name);
      start();
   }

   public void run() {
      try {
         for(int loop_index = 0; loop_index < 2; loop_index++) {
            System.out.println((Thread.currentThread()).getName()
                               + " hilo aqui...");
            Thread.sleep(1000);
         }
      } catch (InterruptedException e) {}
      System.out.println((Thread.currentThread()).getName()
      + " finaliza.");
   }

   public static void main(String args[]) {
      HiloMultipleThread thread1 = new HiloMultipleThread("primero");
      HiloMultipleThread thread2 = new HiloMultipleThread("segundo");
      HiloMultipleThread thread3 = new HiloMultipleThread("tercero");
      HiloMultipleThread thread4 = new HiloMultipleThread("cuarto");
      try {
         for(int loop_index = 0; loop_index < 2; loop_index++) {
            System.out.println((Thread.currentThread()).getName()
                               + " hilo aqui...");
            Thread.sleep(1000);
         }
      } catch (InterruptedException e) {}
   }
}

/* EJECUCION:
primero hilo aqui...
segundo hilo aqui...
tercero hilo aqui...
main hilo aqui...
cuarto hilo aqui...
primero hilo aqui...
segundo hilo aqui...
tercero hilo aqui...
main hilo aqui...
cuarto hilo aqui...
primero finaliza.
segundo finaliza.
tercero finaliza.
cuarto finaliza.
*/
