package prg1210aldarias;

class SecondThread5 implements Runnable {

  Thread thread;

  SecondThread5() {
    thread = new Thread(this, "segundo");
    System.out.println("Iniciando segundo hilo");
    thread.start();
  }

  public void run() {
    try {
      for (int loop_index = 0; loop_index < 2; loop_index++) {
        System.out.println((Thread.currentThread()).getName()
                + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
    }
    System.out.println("Final del segundo hilo.");
  }
}

class runnable {

  public static void main(String args[]) {
    SecondThread secondthread = new SecondThread();
    try {
      for (int loop_index = 0; loop_index < 2; loop_index++) {
        System.out.println((Thread.currentThread()).getName()
                + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
    }
  }
}
/* EJEMPLO:
 Iniciando segundo hilo
 main hilo aqui...
 segundo hilo aqui...
 main hilo aqui...
 segundo hilo aqui...
 Final del segundo hilo.
 */
