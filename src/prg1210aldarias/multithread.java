package prg1210aldarias;

class CustomThread3 extends Thread {

  CustomThread3(String name) {
    super(name);
    start();
  }

  public void run() {
    try {
      for (int loop_index = 0; loop_index < 2; loop_index++) {
        System.out.println((Thread.currentThread()).getName()
                + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
    }
    System.out.println((Thread.currentThread()).getName()
            + " finaliza.");
  }
}

class multithread3 {

  public static void main(String args[]) {
    CustomThread thread1 = new CustomThread("primero");
    CustomThread thread2 = new CustomThread("segundo");
    CustomThread thread3 = new CustomThread("tercero");
    CustomThread thread4 = new CustomThread("cuarto");
    try {
      for (int loop_index = 0; loop_index < 2; loop_index++) {
        System.out.println((Thread.currentThread()).getName()
                + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
    }
  }
}

/* EJECUCION:
 primero hilo aqui...
 segundo hilo aqui...
 tercero hilo aqui...
 main hilo aqui...
 cuarto hilo aqui...
 primero hilo aqui...
 segundo hilo aqui...
 tercero hilo aqui...
 main hilo aqui...
 cuarto hilo aqui...
 primero finaliza.
 segundo finaliza.
 tercero finaliza.
 cuarto finaliza.
 */
