package prg1210aldarias;

class CustomThread5 extends Thread {

  CustomThread5(String name) {
    super(name);
    start();
  }

  public void run() {
    try {
      for (int loop_index = 0; loop_index < 2; loop_index++) {
        System.out.println((Thread.currentThread()).getName()
                + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
    }
    System.out.println((Thread.currentThread()).getName()
            + " finaliza.");
  }
}

class join {

  public static void main(String args[]) {
    CustomThread5 thread1 = new CustomThread5("primer");
    CustomThread5 thread2 = new CustomThread5("segundo");

    try {
      thread1.join();
      thread2.join();

    } catch (InterruptedException e) {
    }
  }
}
/* EJECICION:
 primer hilo aqui...
 segundo hilo aqui...
 primer hilo aqui...
 segundo hilo aqui...
 primer finaliza.
 segundo finaliza.
 */
