package mensaje;

//InterfazReceptorMensajes.java
import java.rmi.*;

/**
 * @author casidiablo
 *
 */
public interface InterfazReceptorMensajes extends Remote {
  //Este es el metodo que implementará el servidor

  void recibirMensaje(String texto) throws RemoteException;
}
