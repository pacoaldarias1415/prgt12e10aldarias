package suma;

/* Servidor */
import java.rmi.*;
import java.net.InetAddress;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;

/**
 * Servidor para el ejemplo de RMI. Exporte un metodo para sumar dos
 * enteros y devuelve el resultado.
 */
public class Servidor {

  /**
   * Crea nueva instancia de Servidor rmi
   */
  public Servidor() {
    try {
      int estePuerto;
      String estaIP;
      Registry registro;
      InterfaceRemota objetoRemoto;
      try {
        // obtener la direccion de este host.
        estaIP = (InetAddress.getLocalHost()).toString();
      } catch (Exception e) {
        throw new RemoteException("No se puede obtener la direccion IP.");
      }
      estePuerto = 3232;

      //crear registro en el puerto
      registro = LocateRegistry.createRegistry(estePuerto);

      // enlazar (rebind) con el servidor
      objetoRemoto = new ObjetoRemoto();
      registro.rebind("rmiServidor", objetoRemoto);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    new Servidor();
  }
}
