package suma;

/* Cliente.java */
import java.rmi.*;
import java.rmi.registry.*;

public class Cliente {

  /**
   * Crea nueva instancia de Cliente
   */
  public Cliente() {
    try {
      Registry registro;
      String direccionServidor = "192.168.1.37";
      int puertoServidor = 3232;
      InterfaceRemota objetoRemoto;

      // obtener el registro
      registro = LocateRegistry.getRegistry(direccionServidor, puertoServidor);

      // creando el objeto remoto
      objetoRemoto = (InterfaceRemota) (registro.lookup("rmiServidor"));

      // Se realiza la suma remota.
      System.out.print("2 + 3 = ");
      System.out.println(objetoRemoto.suma(2, 3));
    } catch (RemoteException e) {
      e.printStackTrace();
    } catch (NotBoundException e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    new Cliente();
  }
}
