package suma;

/* En cliente */
import java.rmi.*;
//import java.io.Serializable;

/**
 * Interface remota con los metodos que se podran llamar en remoto
 */
public interface InterfaceRemota extends Remote {

  public int suma(int a, int b) throws RemoteException;
}
